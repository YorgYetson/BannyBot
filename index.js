const Discord = require("discord.js");
const client = new Discord.Client({
  partials: ["MESSAGE", "CHANNEL", "REACTION"],
});

// The bot will only be active in this channel ID
const welcomeChannel = process.env.channel;

// Bans anyone without a role if they react to a message in welcomeChannel
client.on("messageReactionAdd", async (reaction, user) => {
  // When we receive a reaction we check if the reaction is partial or not
  if (reaction.partial) {
    // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
    try {
      await reaction.fetch();
    } catch (error) {
      console.error("Something went wrong when fetching the message: ", error);
      // Return as `reaction.message.author` may be undefined/null
      return;
    }
  }

  // The Incredible Hulk
  if (reaction.message.channel.id === welcomeChannel) {
    const guildMember = reaction.message.guild.members.cache.get(user.id);

    if (guildMember.roles.cache.size === 1) {
      guildMember.ban();
    }
  }
});

// TODO: server takeover code

client.login(process.env.token);
